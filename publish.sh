#!/usr/bin/env bash
set -e
source .env

./export.sh

pushd static
git add -A
git commit -m "Update on the website at $(date)"
git push origin master
popd