#!/usr/bin/env bash
set -e
source .env

docker-compose run buster
docker-compose run fixlinks
docker-compose run fixrss