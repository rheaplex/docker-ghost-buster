#!/usr/bin/env bash

set -e
source .env


if [ $# -ne 1 ]; then
    echo "$0: usage: ./restore_config.sh <Ghost files backup archive>"
    exit 1
fi

echo "About to extract config.production.json from $BACKUP_PATH/ghost_backups/$1"
tar xzvf $BACKUP_PATH/ghost_backups/$1 config.production.json

