#!/usr/bin/env bash

echo "Fixing links to https"
find static -name *.html -type f -exec sed -i -e "s#http://localhost:2368#https://${REMOTE_DOMAIN}#g" {} \;
find static -name *.html -type f -exec sed -i -e "s#http://fonts.googleapis.com#https://fonts.googleapis.com#" {} \;
find static -name *.html -type f -exec sed -i -e "s#http://code.jquery.com#https://code.jquery.com#" {} \;

echo "Fixing some blog posts"
find static -name *.html -type f -exec sed -i -e "s#open https://${REMOTE_DOMAIN}#open http://localhost:2368#g" {} \;
find static -name *.html -type f -exec sed -i -e "s#domain=https://${REMOTE_DOMAIN}#domain=http://localhost:2368#g" {} \;
find static -name *.html -type f -exec sed -i -e "s#s\#https://${REMOTE_DOMAIN}#s\#http://localhost:2368#g" {} \;

echo "Removing index.html from links"
find static -name *.html -type f -exec sed -i -e "s#/index.html#/#g" {} \;

echo "Removing query string from files"
for i in `find static -name "*?v=*" -type f`
do
    mv $i `echo $i | cut -d? -f1`
done

