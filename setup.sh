#!/usr/bin/env bash
set -e
source .env

# git cloning Casper theme as a git submodule
git submodule update

# create directory structure, GitLab CI config and dummy index file
mkdir -p static/public
cp sample-gitlab-ci.yml static/.gitlab-ci.yml
cp sample-readme.md static/README.md
test -f "static/public/index.html" || cp index.html static/public/

# set up git repository for GITLAB
pushd static
if [ ! -d .git ]; then
	git init
	git add -A
	git commit -m "Initialising the static directory"
	git push --set-upstream $GIT_REMOTE master
	git remote add origin $GIT_REMOTE
fi
popd

