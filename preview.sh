#!/usr/bin/env bash
set -e
source .env

./export.sh

docker-compose up -d preview
sleep 10
open http://localhost:${PREVIEW_PORT}
